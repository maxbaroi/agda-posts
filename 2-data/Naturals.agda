{-# OPTIONS --without-K --exact-split --safe #-}


module Naturals where

open import Utilities public
open import Booleans public

data ℕ : Set where
  Z : ℕ
  S : ℕ → ℕ

_+_ _×_ : ℕ → ℕ → ℕ

m + Z = m
m + S n = S (m + n)

m × Z = Z
m × S n = m + m × n

infixl 20 _+_
infixr 21 _×_

{-# BUILTIN NATURAL ℕ #-}

ℕ-induction : (A : ℕ → Set ℓ)
            → A Z
            → ((n : ℕ) → A n → A (S n))
            → (n : ℕ) → A n
ℕ-induction A a₀ f Z = a₀
ℕ-induction A a₀ f (S n) = f n ih
  where
    ih = ℕ-induction A a₀ f n

ℕ-recursion : (B : Set ℓ)
            → B
            → (ℕ → B → B)
            → ℕ → B
ℕ-recursion B = ℕ-induction λ _ → B

ℕ-iterator : (B : Set ℓ)
           → B
           → (B → B)
           → ℕ → B
ℕ-iterator B b₀ f = ℕ-recursion B b₀ λ _ → f


module Naturals-Iteration where

  _+'_ _×'_ : ℕ → ℕ → ℕ

  _+'_  = λ m → ℕ-iterator ℕ m S 
  _×'_ = λ m → ℕ-iterator ℕ Z (m +_)


_≤?_ : ℕ → ℕ → 𝔹
Z ≤? _ = tt
S _ ≤? Z = ff
S m ≤? S n = m ≤? n

_≤_ _≥_ : ℕ → ℕ → Set
Z ≤ _ = 𝟙
S _ ≤ Z = 𝟘
S m ≤ S n = m ≤ n

m ≥ n = n ≤ n

≤-implies-≤? : (m n : ℕ) → (m ≤ n) → (m ≤? n) ≡ tt
≤-implies-≤? Z _ _ = refl _
≤-implies-≤? (S m) (S n) p = ≤-implies-≤? m n p

≤?-implies-≤ : (m n : ℕ) → (m ≤? n) ≡ tt → m ≤ n
≤?-implies-≤ Z Z _ = *
≤?-implies-≤ Z (S _) _ = *
≤?-implies-≤ (S m) (S n) p = ≤?-implies-≤ m n p


data _≤'_ : ℕ → ℕ → Set where
  LTE-Z : {n : ℕ} → Z ≤' n
  LTE-S : {m n : ℕ} → m ≤' n → S m ≤' S n

≤-implies-≤' : (m n : ℕ) → m ≤ n → m ≤' n
≤-implies-≤' Z n _ = LTE-Z
≤-implies-≤' (S m) (S n) p = LTE-S (≤-implies-≤' m n p)


LTE-S-injective : {m n : ℕ} → S m ≤' S n → m ≤' n
LTE-S-injective (LTE-S p) = p

≤'-implies-≤ : (m n : ℕ) → m ≤' n → m ≤ n
≤'-implies-≤ Z n _ = *
≤'-implies-≤ (S m) (S n) p = ≤'-implies-≤ m n (LTE-S-injective p)


≤-iff-≤' : (m n : ℕ) → (m ≤ n) ⇔ (m ≤' n)
≤-iff-≤' m n = ≤-implies-≤' m n , ≤'-implies-≤ m n


_≼_ : ℕ  → ℕ → Set  
m ≼ n = Σ[ o ∈ ℕ ] ((m + o) ≡ n)


+-Z-r : (n : ℕ) → (n + Z) ≡ n
+-Z-r _ = refl _

+-Z-l : (n : ℕ) → (Z + n) ≡ n
+-Z-l Z = refl _
+-Z-l (S n) =
  (Z + S n) ≡⟨ refl _ ⟩
  S (Z + n) ≡⟨ ap S ih ⟩
  (S n ∎)
  where
    ih : (Z + n) ≡ n
    ih = +-Z-l n

+-S-r : (m n : ℕ) → (m + S n) ≡ S (m + n)
+-S-r _ _ = refl _

+-S-l : (m n : ℕ) → (S m + n) ≡ S (m + n)
+-S-l m Z = refl _
+-S-l m (S n) = ap S (+-S-l m n)

pred : ℕ → ℕ
pred Z = Z
pred (S n) = n

S-inj : {m n : ℕ} → S m ≡ S n → m ≡ n
S-inj {Z} {Z} p = refl _
S-inj {S m} {S n} p = ap pred p


S-≢-Z : (n : ℕ) → S n ≢ Z
S-≢-Z n p = 𝟙-≢-𝟘 (ap f p)
  where
    f : ℕ → Set
    f Z = 𝟘
    f (S n) = 𝟙


≼-implies-≤ : (m n : ℕ) → m ≼ n → m ≤ n
≼-implies-≤ Z _ _ = *
≼-implies-≤ (S m) Z (k , p) = S-≢-Z (m + k)
                                    (S (m + k) ≡⟨ +-S-l m k ⁻¹ ⟩
                                    S m + k ≡⟨ p ⟩
                                    (Z ∎))
≼-implies-≤ (S m) (S n) (k , p) = ≼-implies-≤ m n (k , ap pred q)
  where
    q : S (m + k) ≡ S n
    q = S (m + k) ≡⟨ +-S-l m k ⁻¹ ⟩
        S m + k ≡⟨ p ⟩
        (S n ∎) 



≤-implies-≼ : (m n : ℕ) → m ≤ n → m ≼ n
≤-implies-≼ Z n p = n , +-Z-l n
≤-implies-≼ (S m) (S n) p = k , (S m + k ≡⟨ +-S-l m k ⟩
                                S (m + k) ≡⟨ ap S q ⟩
                                (S n ∎))
  where
    ih : m ≼ n
    ih = ≤-implies-≼ m n p

    k : ℕ
    k = fst ih

    q : (m + k) ≡ n
    q = snd ih


≤-iff-≼ : (m n : ℕ) → (m ≤ n) ⇔ (m ≼ n)
≤-iff-≼ m n = ≤-implies-≼ m n , ≼-implies-≤ m n 

+-assoc : (m n o : ℕ) → (m + (n + o)) ≡ (m + n + o)
+-assoc m n Z = refl _
+-assoc m n (S o) =
  m + (n + S o) ≡⟨ ap S (+-assoc m n o) ⟩
  ((m + n + S o) ∎)

+-comm : (m n : ℕ) → (m + n) ≡ (n + m)
+-comm Z n = +-Z-l n
+-comm (S m) n =
  S m + n ≡⟨ +-S-l m n ⟩
  S (m + n) ≡⟨ ap S (+-comm m n) ⟩
  S (n + m) ≡⟨ refl _ ⟩
  ((n + S m) ∎)

×-Z-r : (n : ℕ) → (n × Z) ≡ Z
×-Z-r _ = refl _

×-Z-l : (n : ℕ) → (Z × n) ≡ Z
×-Z-l Z = refl _
×-Z-l (S n) =
  Z × S n ≡⟨ refl _ ⟩
  Z + Z × n ≡⟨ +-Z-l (Z × n) ⟩
  Z × n ≡⟨ ×-Z-l n ⟩
  (Z ∎)

×-S-l : (m n : ℕ) → (S m × n) ≡ (n + m × n)
×-S-l m Z = refl _
×-S-l m (S n) =
  S m × S n ≡⟨ refl _ ⟩
  S m + S m × n ≡⟨ ap (λ k → S m + k) (×-S-l m n) ⟩
  S m + (n + m × n) ≡⟨ +-assoc (S m) n (m × n) ⟩
  S m + n + m × n ≡⟨ ap (λ k → k + m × n) q ⟩
  S n + m + m × n ≡⟨ +-assoc (S n) m (m × n) ⁻¹ ⟩
  S n + (m + m × n) ≡⟨ refl _ ⟩
  ((S n + m × S n) ∎)
  where
    q : (S m + n) ≡ (S n + m)
    q = S m + n ≡⟨ +-S-l m n ⟩
        S (m + n) ≡⟨ ap S (+-comm m n) ⟩
        S (n + m) ≡⟨ +-S-l n m ⁻¹ ⟩
        ((S n + m) ∎)

×-1-r : (k : ℕ) → (k × 1) ≡ k
×-1-r _ = refl _

×-1-l : (k : ℕ) →  1 × k ≡ k
×-1-l Z = refl _
×-1-l (S k) = 1 + 1 × k ≡⟨ +-comm 1 (1 × k) ⟩
              1 × k + 1 ≡⟨ ap S (×-1-l k) ⟩
              (S k ∎)

×-comm : (m n : ℕ) → (m × n) ≡ (n × m)
×-comm Z n = ×-Z-l n
×-comm (S m) n = S m × n ≡⟨ ×-S-l m n ⟩
                 n + m × n ≡⟨ ap (λ k → n + k) (×-comm m n)  ⟩ 
                 n + n × m ≡⟨ refl _ ⟩
                 ((n × S m) ∎)

×-l-dist-over-+ : (k m n : ℕ) → (k × (m + n)) ≡ (k × m + k × n)
×-l-dist-over-+ k m Z = refl _
×-l-dist-over-+ k m (S n) =
  k × (m + S n) ≡⟨ refl _ ⟩
  k + k × (m + n) ≡⟨ ap (λ l → k + l ) (×-l-dist-over-+ k m n) ⟩
  k + (k × m + k × n)  ≡⟨ +-assoc k (k × m) (k × n) ⟩
  k + k × m + k × n ≡⟨ ap (λ l → l + k × n) (+-comm k (k × m)) ⟩
  k × m + k + k × n ≡⟨ +-assoc (k × m) k (k × n) ⁻¹ ⟩
  k × m + (k + k × n) ≡⟨ refl _ ⟩
  ((k × m + k × S n) ∎)

×-r-dist-over-+ : (k m n : ℕ) → ((m + n) × k) ≡ (m × k + n × k)
×-r-dist-over-+ k m n =
  (m + n) × k ≡⟨ ×-comm (m + n) k ⟩
  k × (m + n) ≡⟨ ×-l-dist-over-+ k m n ⟩
  k × m + k × n ≡⟨ ap (λ l → l + k × n) (×-comm k m) ⟩
  m × k + k × n ≡⟨ ap (λ l → m × k + l) (×-comm k n) ⟩
  ((m × k + n × k) ∎)

×-assoc : (m n o : ℕ) → ((m × n) × o) ≡ (m × n × o)
×-assoc m n Z = refl _
×-assoc m n (S o) =
  (m × n) × S o ≡⟨ refl _ ⟩
  m × n + (m × n) × o ≡⟨ ap (λ k → m × n + k) (×-assoc m n o) ⟩
  m × n + m × n × o ≡⟨ ×-l-dist-over-+ m n (n × o) ⁻¹ ⟩
  m × (n + n × o) ≡⟨ refl _ ⟩
  ((m × n × S o) ∎)

+-r-cancel : (k m n : ℕ) → (m + k) ≡ (n + k) → m ≡ n
+-r-cancel Z m n p = p
+-r-cancel (S k) m n p = +-r-cancel k m n (S-inj p)

+-l-cancel : (k m n : ℕ) → (k + m) ≡ (k + n) → m ≡ n
+-l-cancel k m n p = +-r-cancel k m n
                                (m + k ≡⟨ +-comm m k ⟩
                                k + m ≡⟨ p ⟩
                                k + n ≡⟨ +-comm k n ⟩
                                ((n + k) ∎))


0-only-×-zero-divisor : (m n : ℕ) → (m × n) ≡ Z → (m ≡ Z) ∨ (n ≡ Z)
0-only-×-zero-divisor Z _ _ = inl (refl _)
0-only-×-zero-divisor (S _) Z _ = inr (refl _)
0-only-×-zero-divisor (S m) (S n) p = !𝟘 (S-≢-Z (m + S m × n) q)
  where
    q : (S (m + S m × n)) ≡ Z
    q = S (m + S m × n) ≡⟨ +-S-l m (S m × n) ⁻¹ ⟩
        S m + S m × n ≡⟨ p ⟩
        (Z ∎)

×-l-cancel : (k m n : ℕ) → (S k × m) ≡ (S k × n) → m ≡ n
×-l-cancel k m Z p = ∨-l-elim (0-only-×-zero-divisor (S k) m p) (S-≢-Z k)
×-l-cancel k Z (S n) p = !𝟘 (S-≢-Z (k + S k × n) q)
  where
    q : S (k + S k × n) ≡ Z
    q = S (k + S k × n) ≡⟨ +-S-l k (S k × n) ⁻¹ ⟩
        S k + S k × n ≡⟨ p ⁻¹ ⟩
        (Z ∎)
×-l-cancel k (S m) (S n) p = ap S (×-l-cancel k m n (+-l-cancel (S k) (S k × m) (S k × n) p))

×-r-cancel : (k m n : ℕ) → (m × S k) ≡ (n × S k) → m ≡ n
×-r-cancel k m n p = ×-l-cancel k m n q
  where
    q : (S k × m) ≡ (S k × n)
    q = S k × m ≡⟨ ×-comm (S k) m ⟩
        m × S k ≡⟨ p ⟩
        n × S k ≡⟨ ×-comm n (S k) ⟩
        ((S k × n) ∎)


0-only-ℕ-+-unit-divisor : (m n : ℕ) → (m + n) ≡ Z → (m ≡ Z) ∧ (n ≡ Z)
0-only-ℕ-+-unit-divisor Z Z p = refl _ , refl _

1-only-ℕ-×-unit-divisor : (m n : ℕ) → (m × n) ≡ 1 → (m ≡ 1) ∧ (n ≡ 1)
1-only-ℕ-×-unit-divisor Z (S n) p = !𝟘 (S-≢-Z Z q)
  where
    q : 1 ≡ 0
    q = 1 ≡⟨ p ⁻¹ ⟩
        Z × S n ≡⟨ ×-Z-l (S n) ⟩
        (0 ∎)
1-only-ℕ-×-unit-divisor (S m) (S n) p = ap S (fst η) , ap S q'
  where
    q : S (m + S m × n) ≡ 1
    q = S (m + S m × n) ≡⟨ +-S-l m (S m × n) ⁻¹ ⟩
        S m × S n ≡⟨ p ⟩
        (1 ∎)

    η : (m ≡ 0) ∧ ((S m × n) ≡ 0)
    η = 0-only-ℕ-+-unit-divisor m (S m × n) (ap pred q)

    q' : n ≡ 0
    q' = n ≡⟨ ×-1-l n ⁻¹ ⟩
          S Z × n ≡⟨ ap (λ - → S - × n) (fst η) ⁻¹ ⟩
          S m × n ≡⟨ snd η ⟩
          (0 ∎)


is-prime : ℕ → Set
is-prime n = (n ≥ 2) ∧ ((k m : ℕ) → (k × m) ≡ n → (k ≡ 1) ∨ (k ≡ n))

twin-prime-conjecture : Set
twin-prime-conjecture = (n : ℕ) → Σ[ p ∈ ℕ ] ((p ≥ n) ∧ is-prime p ∧ is-prime (S (S p)))

goldbach-conjecture : Set
goldbach-conjecture = (n : ℕ) → n ≥ 3 → Σ[ p₁ ∈ ℕ ] Σ[ p₂ ∈ ℕ ] (is-prime p₁ ∧ is-prime p₂ ∧ ((2 × n) ≡ (p₁ + p₂)))


≤-refl : is-refl _≤_
≤-refl Z = *
≤-refl (S k) = ≤-refl k

≤-anti : is-anti _≤_
≤-anti Z Z p q = refl _
≤-anti (S m) (S n) p q = ap S (≤-anti m n p q)

≤-tran : is-trans _≤_
≤-tran Z m n p q = *
≤-tran (S k) (S m) (S n) p q = ≤-tran k m n p q

_div_ : ℕ → ℕ → Set
m div n = Σ[ k ∈ ℕ ] ((k × m) ≡ n)


div-tran : is-trans _div_
div-tran m n o (k , p) (l , q) = l × k ,
                                 ((l × k) × m ≡⟨ ×-assoc l k m ⟩
                                 l × k × m ≡⟨ ap (λ -  → l × -) p ⟩
                                 l × n ≡⟨ q ⟩
                                 (o ∎))


div-refl : is-refl _div_
div-refl n = 1 , ×-1-l n


div-anti : is-anti _div_
div-anti Z Z (k , p) (l , q) = refl _
div-anti m@(S m') n@(S n') (k , p) (l , q) = m ≡⟨ q ⁻¹ ⟩
                                             l × n ≡⟨ ap (λ - → - × n) (snd q''') ⟩
                                             1 × n ≡⟨ ×-1-l n ⟩
                                             (n ∎)
  where
    q' : ((k × l) × n) ≡ (1 × n)
    q' = (k × l) × n ≡⟨ ×-assoc k l n ⟩
         k × l × n ≡⟨ ap (λ - → k × -) q ⟩
         k × m ≡⟨ p ⟩
         n ≡⟨ ×-1-l n ⁻¹ ⟩
         ((1 × n) ∎)

    q'' : (k × l) ≡ 1
    q'' = ×-r-cancel n' (k × l) 1 q'

    q''' : (k ≡ 1) ∧ (l ≡ 1)
    q''' = 1-only-ℕ-×-unit-divisor k l q''


≤'-iff-≼ : (m n : ℕ) → (m ≤' n) ⇔  (m ≼ n)
≤'-iff-≼ m n = ⇔-tran (⇔-symm (≤-iff-≤' m n)) (≤-iff-≼ m n)

_<_ _>_ : ℕ → ℕ → Set
m < n = S m ≤ n
m > n = n < m


zero-minimal : (n : ℕ) → n ≤ Z → n ≡ Z
zero-minimal Z p = refl _

≤-S : (k : ℕ) → k ≤ S k
≤-S Z = *
≤-S (S k) = ≤-S k

≤-+-r : (k m n : ℕ) → k ≤ m → k ≤ (m + n)
≤-+-r k m Z p = p
≤-+-r k m (S n) p = ≤-tran k (m + n) (m + S n) (≤-+-r k m n p) (≤-S (m + n))
  where
    f : (o : ℕ) → o ≤ S o
    f Z = *
    f (S o) = f o

≤-+-monotonic : (k m n : ℕ) → k ≤ m → (k + n) ≤ (m + n)
≤-+-monotonic k m Z p = p
≤-+-monotonic k m (S n) p = ≤-+-monotonic k m n p

≤-+-combine : (k l m n : ℕ) → k ≤ l → m ≤ n → (k + m) ≤ (l + n)
≤-+-combine k l Z n p q = ≤-+-r k l n p
≤-+-combine k l (S m) (S n) p q = ≤-+-combine k l m n p q

≤-×-monotonic : (k m n : ℕ) → m ≤ n → (m × k) ≤ (n × k)
≤-×-monotonic Z m n p = *
≤-×-monotonic (S k) m n p = ≤-+-combine m n (m × k) (n × k) p (≤-×-monotonic k m n p)

≤-×-combine : (k l m n : ℕ) → k ≤ l → m ≤ n → (k × m) ≤ (l × n)
≤-×-combine k l Z n p q = *
≤-×-combine k l (S m) (S n) p q = ≤-+-combine k l (k × m) (l × n) p (≤-×-combine k l m n p q)

ℕ-has-decidable-equality : has-decidable-equality ℕ
ℕ-has-decidable-equality Z Z = inl (refl _)
ℕ-has-decidable-equality Z (S n) = inr (≢-sym (S-≢-Z n))
ℕ-has-decidable-equality (S m) Z = inr (S-≢-Z m)
ℕ-has-decidable-equality (S m) (S n) = (f →∨ g) (ℕ-has-decidable-equality m n)
  where
    f : m ≡ n → S m ≡ S n
    f = ap S

    g : ¬ (m ≡ n) → ¬ (S m ≡ S n)
    g u p = u (ap pred p)


ℕ-trichotomy : (m n : ℕ) → (m ≡ n) ∨ (m < n) ∨ (m > n)
ℕ-trichotomy Z Z = inl (refl _)
ℕ-trichotomy Z (S n) = inr (inl *)
ℕ-trichotomy (S m) Z = inr (inr *)
ℕ-trichotomy (S m) (S n) = (ap S →∨ id) (ℕ-trichotomy m n)


≤-S-r : (n : ℕ) → n ≤ S n
≤-S-r Z = *
≤-S-r (S n) = ≤-S-r n

split-case-≤ : (m n : ℕ) → m ≤ n → (m ≡ n) ∨ (m < n)
split-case-≤ Z Z p = inl (refl _)
split-case-≤ Z (S n) p = inr *
split-case-≤ (S m) (S n) p = (ap S →∨ id) (split-case-≤ m n p)

data _≤''_ : ℕ → ℕ → Set where
  LTE-= : {n : ℕ} → n ≤'' n
  LTE-inc : {m n : ℕ} → m ≤'' n → m ≤'' S n

≤-implies-≤'' : (m n : ℕ) → m ≤ n → m ≤'' n
≤-implies-≤'' Z Z p = LTE-=
≤-implies-≤'' Z (S n) p = LTE-inc (≤-implies-≤'' Z n p)
≤-implies-≤'' (S m) (S n) p with split-case-≤ m n p
≤-implies-≤'' (S m) (S n) p | inl q = transport (λ l → S m ≤'' S l) q LTE-=
≤-implies-≤'' (S m) (S n) p | inr q = LTE-inc (≤-implies-≤'' (S m) n q)

≤''-implies-≤ : (m n : ℕ) → m ≤'' n → m ≤ n
≤''-implies-≤ m .m LTE-= = ≤-refl m
≤''-implies-≤ m .(S _) (LTE-inc q) = ≤-tran m n (S n) (≤''-implies-≤ m _ q) (≤-S-r n)
  where
    rhs-≤'' : {m n : ℕ} → m ≤'' n → ℕ
    rhs-≤'' {m} {n} q = n

    n : ℕ
    n = rhs-≤'' q

≤-iff-≤'' : (m n : ℕ) → (m ≤ n) ⇔ (m ≤'' n)
≤-iff-≤'' m n = ≤-implies-≤'' m n , ≤''-implies-≤ m n 

double : ℕ → ℕ
double Z = Z
double (S n) = S (S (double n))

data Even : ℕ → Set where
  Even-Z : Even Z
  Even-SS : {n : ℕ} → Even n → Even (S (S n))

is-even : ℕ → Set
is-even Z = 𝟙
is-even (S Z) = 𝟘
is-even (S (S n)) = is-even n

is-even-2 : ℕ → Set
is-even-2 n = Σ[ k ∈ ℕ ] ((2 × k) ≡ n)

is-even-d : ℕ → Set
is-even-d n = Σ[ k ∈ ℕ ] (double k ≡ n)

is-even-implies-i : (n : ℕ) → is-even n → Even n
is-even-implies-i Z p = Even-Z
is-even-implies-i (S (S n)) p = Even-SS (is-even-implies-i n p)

is-even-implies-2 : (n : ℕ) → is-even n → is-even-2 n
is-even-implies-2 Z p = Z , refl _
is-even-implies-2 (S (S n)) p with is-even-implies-2 n p
is-even-implies-2 (S (S n)) p | k , q = S k ,
  (2 × S k ≡⟨ refl _ ⟩
  2 + 2 × k ≡⟨ +-S-l 1 (2 × k) ⟩
  S (1 + 2 × k) ≡⟨ ap S (+-S-l 0 (2 × k)) ⟩
  S (S (Z + 2 × k)) ≡⟨  ap (S ∘ S) (+-Z-l (2 × k)) ⟩
  S (S (2 × k)) ≡⟨ ap (S ∘ S) q ⟩
  (S (S n) ∎))

is-even-implies-d : (n : ℕ) → is-even n → is-even-d n
is-even-implies-d Z p = Z , refl _
is-even-implies-d (S (S n)) p with is-even-implies-d n p
is-even-implies-d (S (S n)) p | k , q = S k ,
  (double (S k) ≡⟨ refl _ ⟩
  S (S (double k)) ≡⟨ ap (S ∘ S) q ⟩
  (S (S n) ∎))

Even-implies-is-even : (n : ℕ) → Even n → is-even n
Even-implies-is-even .0 Even-Z = *
Even-implies-is-even .(S (S _)) (Even-SS p) = Even-implies-is-even _ p
    
