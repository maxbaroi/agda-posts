{-# OPTIONS --without-K --exact-split --safe #-}

module Utilities where

open import Agda.Primitive public

variable
  ℓ ℓ' ℓ'' : Level

data 𝟙 : Set where
  * : 𝟙

𝟙-induction : (A : 𝟙 → Set ℓ) → A *
            → (x : 𝟙) → A x
𝟙-induction A a * = a            

𝟙-recursion : (B : Set ℓ) → B
            → 𝟙 → B
𝟙-recursion B  = 𝟙-induction (λ _ → B)            

!𝟙 : {B : Set ℓ} → B → 𝟙
!𝟙 _ = *

!𝟙' : (B : Set ℓ) → B → 𝟙
!𝟙' _ = !𝟙

data 𝟘 : Set where

𝟘-induction : (A : 𝟘 → Set ℓ)
            → (x : 𝟘) → A x
𝟘-induction A ()

𝟘-recursion : (B : Set ℓ)
            → 𝟘 → B
𝟘-recursion B = 𝟘-induction λ _ → B            

!𝟘 : {B : Set ℓ} → 𝟘 → B
!𝟘 {ℓ} {B} = 𝟘-recursion B

!𝟘' : (B : Set ℓ) → 𝟘 → B
!𝟘' _ = !𝟘

is-empty : Set ℓ → Set ℓ
is-empty X = X → 𝟘

¬ : Set ℓ → Set ℓ
¬ X = is-empty X

data Id (X : Set ℓ) : X → X → Set ℓ where
 refl : (x : X) → Id X x x

_≡_ : {X : Set ℓ} → X → X → Set ℓ
x ≡ y = Id _ x y

Id-induction : (X : Set ℓ) (A : (x y : X) → x ≡ y → Set ℓ')
             → ((x : X) → A x x (refl x))
             → (x y : X) (p : x ≡ y) → A x y p
Id-induction X A f x .x (refl .x) = f x


𝕁 : (X : Set ℓ) (A : (x y : X) → x ≡ y → Set ℓ')
  → ((x : X) → A x x (refl x))
  → (x y : X) (p : x ≡ y) → A x y p
𝕁 = Id-induction


_∘_ : {X : Set ℓ} {Y : Set ℓ'} {Z : Y → Set ℓ''}
    → ((y : Y) → Z y)
    → (f : X → Y)
    → (x : X) → Z (f x)
g ∘ f = λ x → g (f x)

id : {X : Set ℓ} → X → X
id x = x

id' : (X : Set ℓ) → X → X
id' X = id


lhs : {X : Set ℓ} {x y : X} → x ≡ y → X
lhs {ℓ} {X} {x} {y} p = x

rhs : {X : Set ℓ} {x y : X} → x ≡ y → X
rhs {ℓ} {X} {x} {y} p = y

transport : {X : Set ℓ} (A : X → Set ℓ') {x y : X}
          → x ≡ y → A x → A y
transport A (refl x) = id



transport𝕁 : {X : Set ℓ} (A : X → Set ℓ') {x y : X}
          → x ≡ y → A x → A y
transport𝕁 {ℓ} {ℓ'} {X} A {x} {y} = 𝕁 X (λ x' y' _ → A x' → A y') (λ x' → id' (A x')) x y

transport𝕁-agreement : {X : Set ℓ} (A : X → Set ℓ') {x y : X} (p : x ≡ y)
                     → transport𝕁 A p ≡ transport A p
transport𝕁-agreement A (refl x) = refl (transport A (refl x))

_∙_ : {X : Set ℓ } {x y z : X} → x ≡ y → y ≡ z → x ≡ z
p ∙ q = transport (lhs p ≡_) q p

infixr 20 _∙_

_⁻¹ : {X : Set ℓ} {x y : X} → x ≡ y → y ≡ x
p ⁻¹ = transport (_≡ lhs p) p (refl (lhs p)) 


_∙'_ : {X : Set ℓ} {x y z : X} → x ≡ y → y ≡ z → x ≡ z
p ∙' q = transport (_≡ rhs q) (p ⁻¹) q

∙-agreement : {X : Set ℓ} {x y z : X} (p : x ≡ y) (q : y ≡ z)
            → (p ∙' q) ≡ (p ∙ q)
∙-agreement (refl x) (refl .x) = refl _



ap : {X : Set ℓ} {Y : Set ℓ'} (f : X → Y) {x x' : X}
   → x ≡ x' → f x ≡ f x'
ap f {x} {x'} p = transport (λ - → f x ≡ f -) p (refl (f x))


_≡⟨_⟩_ : {X : Set ℓ} (x : X) {y z : X}
       → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ p ⟩ q = p ∙ q

infixr 3 _≡⟨_⟩_

_∎ : {X : Set ℓ} (x : X) → x ≡ x
x ∎ = refl x

_≢_ : {X : Set ℓ} →  X → X → Set ℓ
x ≢ y = ¬ (x ≡ y)

≢-sym : {X : Set ℓ} {x y : X} → x ≢ y → y ≢ x
≢-sym {ℓ} {X} {x} {y} u p = u (p ⁻¹ )


𝟙-≢-𝟘 : 𝟙 ≢ 𝟘
𝟙-≢-𝟘 p = transport  (id' Set) p *

≡-to-fun : {X Y : Set ℓ} → X ≡ Y → X → Y
≡-to-fun {ℓ} = transport (id' (Set ℓ))

record Σ {X : Set ℓ} (Y : X → Set ℓ') : Set (ℓ ⊔ ℓ') where
  constructor
    _,_
  field
    x : X
    y : Y x

infixr 4 _,_

fst : {X : Set ℓ} {Y : X → Set ℓ'} → Σ Y → X
fst (x , _) = x

snd : {X : Set ℓ} {Y : X → Set ℓ'} → (z : Σ Y) → Y (fst z) 
snd (_ , y) = y

-Σ : (X : Set ℓ) → (Y : X → Set ℓ') → Set (ℓ ⊔ ℓ')
-Σ X Y = Σ Y


syntax -Σ X (λ x → Y) = Σ[ x ∈ X ] Y


_∧_ : Set ℓ → Set ℓ' → Set (ℓ ⊔ ℓ')
X ∧ Y = Σ[ x ∈ X ] Y

infixr 2 _∧_ 

Σ-induction : {X : Set ℓ} {Y : X → Set ℓ'} {A : Σ Y → Set ℓ''}
            → ((x : X) (y : Y x) → A (x , y))
            → ( z : Σ Y)  → A z
Σ-induction g (x , y) = g x y


uncurry : {X : Set ℓ} {Y : X → Set ℓ'} {A : Σ Y → Set ℓ''}
        → ((z : Σ Y) → A z)
         → (x : X) (y : Y x) → A (x , y)
uncurry g x y = g (x , y)

_⇔_ : Set ℓ → Set ℓ' → Set (ℓ ⊔ ℓ')
X ⇔ Y = (X → Y) ∧ (Y → X)

data _∨_ (X : Set ℓ) (Y : Set ℓ') : Set (ℓ ⊔ ℓ') where
  inl : X → X ∨ Y
  inr : Y → X ∨ Y

infixr 1 _∨_

∨-induction : {X : Set ℓ} {Y : Set ℓ'} (A : X ∨ Y → Set ℓ'')
            → ((x : X) → A (inl x))
            → ((y : Y) → A (inr y))
            → (z : X ∨ Y) → A z
∨-induction A f g (inl x) = f x
∨-induction A f g (inr y) = g y            

∨-recursion : {X : Set ℓ} {Y : Set ℓ'} (B : Set ℓ'')
            → (X → B) → (Y → B)
            → X ∨ Y → B
∨-recursion B f g (inl x) = f x
∨-recursion B f g (inr y) = g y            
            

∨-l-elim : {X : Set ℓ} {Y : Set ℓ'}
         → (X ∨ Y) → (¬ X) → Y
∨-l-elim (inl x) u = !𝟘 (u x)
∨-l-elim (inr y) u = y


∨-r-elim : {X : Set ℓ} {Y : Set ℓ'}
         → (X ∨ Y) → (¬ Y) → X
∨-r-elim (inl x) u = x
∨-r-elim (inr y) u = !𝟘 (u y)


_→∨_ : {X X' : Set ℓ} {Y Y' : Set ℓ'} → (X → X') → (Y → Y') → (X ∨ Y) → (X' ∨ Y')
(f →∨ g) (inl x) = inl (f x)
(f →∨ g) (inr y) = inr (g y)

_→∧_ : {X X' : Set ℓ} {Y Y' :  Set ℓ'} → (X → X') → (Y → Y') → (X ∧ Y) → (X' ∧ Y')
(f →∧ g) (x , y) = f x , g y

is-refl : {X : Set ℓ} → ( _R_ : X → X → Set ℓ') → Set (ℓ ⊔ ℓ')
is-refl {ℓ} {ℓ'} {X} _R_ = (x : X) → x R x

≡-refl : {X : Set ℓ} → is-refl {ℓ} {ℓ} {X} _≡_
≡-refl x = refl x

is-symm : {X : Set ℓ} → (_R_ : X → X → Set ℓ') → Set (ℓ ⊔ ℓ')
is-symm {ℓ} {ℓ'} {X} _R_ = (x y : X) → x R y → y R x

≡-symm : {X : Set ℓ} → is-symm {ℓ} {ℓ} {X} _≡_
≡-symm _ _ p = p ⁻¹

is-anti : {X : Set ℓ} → (_R_ : X → X → Set ℓ') → Set (ℓ ⊔ ℓ')
is-anti {ℓ} {ℓ'} {X} _R_ = (x y : X) → x R y → y R x → x ≡ y

is-trans : {X : Set ℓ} → (_R_ : X → X → Set ℓ') → Set (ℓ ⊔ ℓ')
is-trans {ℓ} {ℓ'} {X} _R_ = (x y z : X) → x R y → y R z → x R z

≡-trans : {X : Set ℓ} → is-trans {ℓ} {ℓ} {X} _≡_
≡-trans x y z p q = p ∙ q

⇔-symm : {X Y : Set ℓ} → X ⇔ Y → Y ⇔ X
⇔-symm (f , g) = g , f

⇔-tran : {X Y Z : Set ℓ} → (X ⇔ Y) → (Y ⇔ Z) → X ⇔ Z
⇔-tran (f , g) (f' , g') = (f' ∘ f) , (g ∘ g')



decidable : Set ℓ → Set ℓ
decidable X = X ∨ ¬ X

has-decidable-equality : Set ℓ → Set ℓ
has-decidable-equality X = (x y : X) → decidable (x ≡ y)

𝟙-has-decidable-equality : has-decidable-equality 𝟙
𝟙-has-decidable-equality * * = inl (refl *)

𝟘-has-decidable-equality : has-decidable-equality 𝟘
𝟘-has-decidable-equality a b = !𝟘 a
