{-# OPTIONS --without-K --exact-split --safe #-}

module Utilities where

open import Agda.Primitive public

variable
  ℓ ℓ' ℓ'' : Level

data 𝟙 : Set where
  * : 𝟙

𝟙-induction : (A : 𝟙 → Set ℓ) → A *
            → (x : 𝟙) → A x
𝟙-induction A a * = a            

𝟙-recursion : (B : Set ℓ) → B
            → 𝟙 → B
𝟙-recursion B  = 𝟙-induction (λ _ → B)            

!𝟙 : {B : Set ℓ} → B → 𝟙
!𝟙 _ = *

!𝟙' : (B : Set ℓ) → B → 𝟙
!𝟙' _ = !𝟙

data 𝟘 : Set where

𝟘-induction : (A : 𝟘 → Set ℓ)
            → (x : 𝟘) → A x
𝟘-induction A ()

𝟘-recursion : (B : Set ℓ)
            → 𝟘 → B
𝟘-recursion B = 𝟘-induction λ _ → B            

{-
 We use !𝟘 a lot more than !𝟙 because it sometimes make sense 
  to assume a falsehood, but we never really want to prove vacuous truth
-}

!𝟘 : {B : Set ℓ} → 𝟘 → B
!𝟘 {ℓ} {B} = 𝟘-recursion B

!𝟘' : (B : Set ℓ) → 𝟘 → B
!𝟘' _ = !𝟘

is-empty : Set ℓ → Set ℓ
is-empty X = X → 𝟘

¬ : Set ℓ → Set ℓ
¬ X = is-empty X

data Id (X : Set ℓ) : X → X → Set ℓ where
 refl : (x : X) → Id X x x

{-
  Note equality is different than Idris's builtin
-}

_≡_ : {X : Set ℓ} → X → X → Set ℓ
x ≡ y = Id _ x y
