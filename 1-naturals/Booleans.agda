{-# OPTIONS --without-K --exact-split --safe #-}

module Booleans where

open import Utilities public

data 𝔹 : Set where
  tt ff : 𝔹

{-# BUILTIN BOOL 𝔹 #-}
{-# BUILTIN TRUE tt #-}
{-# BUILTIN FALSE ff #-}

infix 7 ~_
infixl 6 _xor_ _nand_
infixr 6 _&&_
infixr 5 _||_

~_ : 𝔹 → 𝔹
~ tt = ff
~ ff = tt

_&&_ : 𝔹 → 𝔹 → 𝔹
tt && c = c
ff && _ = ff

_||_ : 𝔹 → 𝔹 → 𝔹
tt || _ = tt
ff || c = c

_xor_ : 𝔹 → 𝔹 → 𝔹
tt xor tt = ff
tt xor ff = tt
ff xor tt = tt
ff xor ff = ff

_nand_ : 𝔹 → 𝔹 → 𝔹
tt nand tt = ff
tt nand ff = tt
ff nand _ = tt

_imp_ : 𝔹 → 𝔹 → 𝔹
tt imp b = b
ff imp _ = tt


~-involutive : (b : 𝔹) → (~ (~ b)) ≡ b
~-involutive tt = refl _
~-involutive ff = refl _

xor-anti : (b : 𝔹) → (b xor b) ≡ ff
xor-anti tt = refl _
xor-anti ff = refl _


imp-def : (b c : 𝔹) → (b imp c) ≡ (~ b || c)
imp-def tt tt = refl _
imp-def tt ff = refl _
imp-def ff c = refl _

&&-comm : (b c : 𝔹) → (c && b) ≡ (b && c)
&&-comm tt tt = refl _
&&-comm tt ff = refl _
&&-comm ff tt = refl _
&&-comm ff ff = refl _

||-comm : (b c : 𝔹) → (c || b) ≡ (b || c)
||-comm tt tt = refl _
||-comm tt ff = refl _
||-comm ff tt = refl _
||-comm ff ff = refl _


&&-assoc : (b c d : 𝔹) → ((b && c) && d) ≡ (b && c && d)
&&-assoc tt c d = refl _
&&-assoc ff c d = refl _

||-assoc : (b c d : 𝔹) → ((b || c) || d) ≡ (b || c || d)
||-assoc tt c d = refl _
||-assoc ff c d = refl _


&&-elim-l : (b c : 𝔹) → (b && c) ≡ tt
          → b ≡ tt
&&-elim-l tt c p = refl _

&&-elim-r : (b c : 𝔹) → (b && c) ≡ tt
          → c ≡ tt
&&-elim-r tt tt p = refl _


&&-||-elim : (b c : 𝔹) → (b && c) ≡ (b || c)
             → b ≡ c
&&-||-elim tt tt p = refl _
&&-||-elim ff ff p = refl _


de-morgan-&& : (b c : 𝔹) → (~ (b && c)) ≡ (~ b || ~ c)
de-morgan-&& tt c = refl _
de-morgan-&& ff c = refl _

de-morgan-|| : (b c : 𝔹) → (~ (b || c)) ≡ (~ b && ~ c)
de-morgan-|| tt c = refl _
de-morgan-|| ff c = refl _


~'_ : 𝔹 → 𝔹
~' b = b nand b

_&&'_ : 𝔹 → 𝔹 → 𝔹
b &&' c = ~' (b nand c)

_||'_ : 𝔹 → 𝔹 → 𝔹
b ||' c = (~' b) nand (~' c)


~-equiv : (b : 𝔹) → (~' b) ≡ (~ b)
~-equiv tt = refl _
~-equiv ff = refl _

&&-equiv : (b c : 𝔹) → (b &&' c) ≡ (b && c)
&&-equiv tt tt = refl _
&&-equiv tt ff = refl _
&&-equiv ff c = refl _

||-equiv : (b c : 𝔹) → (b ||' c) ≡ (b || c)
||-equiv tt c = refl _
||-equiv ff tt = refl _
||-equiv ff ff = refl _


